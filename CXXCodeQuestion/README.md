## C++ Multi-Threaded queue

Please provide an implementation of `MTQueue` which uses multiple
threads to complete the workload (estimating PI) as quickly as
possible.

We have provided skeleton C++ code with a single threaded
implementation and a makefile for you.

You may use any standard C++ libraries or anything from boost (http://www.boost.org/)

To run:
```
make
./mtqueue
```

The results should be the same. You are free to change `mtqueue.h` and
`mtqueue.cpp` as you wish but do not change `main.cpp`

Please submit your result via email.



