#include <iostream>
#include <list>

#include <chrono>


#include "mtqueue.h"

// Monte Carlo approach to estimating Pi
class Estimate_pi : public MTQueue::Task
{
public:
    Estimate_pi() : est_(0.0) {}
    void run();
    double get_pi() { return est_; }
private:
    double est_; // current estimate
};

using std::chrono::high_resolution_clock;

class Timer {
public:
    Timer();
    ~Timer();
private:
    high_resolution_clock::time_point start_;
};

int main()
{
    Timer timer;
    MTQueue queue;
    std::cout << "Starting mtqueue driver program..." << std::endl;

    std::list<Estimate_pi> tasks;
    for (size_t i=0; i<100; ++i)
    {
        tasks.push_back(Estimate_pi());
    }

    // Run all tasks in the queue
    for (auto &t : tasks) { queue.add(t); }
    queue.wait_for_empty();

    // print the average result
    double avg = 0.0;
    for (auto &t : tasks) { avg += t.get_pi(); }
    avg /= static_cast<double>(tasks.size());

    std::cout << "Estimate of PI is " << avg << std::endl;
}

#include <RandomLib/Random.hpp>
#include <cstdlib>
#include <math.h>

// simple task
void Estimate_pi::run()
{
    size_t num_tests     = 5000000;
    size_t num_successes = 0;

    RandomLib::Random r;
    r.Reseed();

    for (size_t i=0; i<num_tests; i++)
    {
        // pick random numbers between -1 and 1
        double x = r.FixedS();
        double y = r.FixedS();

        // If in circle
        if (x*x + y*y < 0.25) {
            num_successes++;
        }
    }

    // estimate area in circle
    est_ = static_cast<double>(num_successes) / static_cast<double>(num_tests);
    est_ *= 4.0;
}



Timer::Timer() : start_(high_resolution_clock::now()) {}
Timer::~Timer()
{
    high_resolution_clock::time_point end = high_resolution_clock::now();
    std::chrono::milliseconds total_time =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start_);
    std::cout << "Done in " << total_time.count() << " ms" << std::endl;
}
