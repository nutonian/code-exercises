#include "mtqueue.h"
#include <list>

// Sample implementation does not use threads.
class QueueState
{
public:
    std::list<MTQueue::Task *> task_ptrs_;
};


MTQueue::MTQueue() : state_(new QueueState())
{
}

MTQueue::~MTQueue()
{
    delete state_;
}


void MTQueue::add(Task &t)
{
    state_->task_ptrs_.push_back(&t);
}

void MTQueue::wait_for_empty()
{
    // do the work one by one with this thread
    for (auto t : state_->task_ptrs_)
    {
        t->run();
    }
    state_->task_ptrs_.clear();
}
