#pragma once

class QueueState;

/**
 * Multi-Threaded Queue.
 *
 * The intention is that tasks submitted to an instance of this class will be
 * executed concurrently using multiple threads.
 */
class MTQueue
{
public:
    MTQueue();
    ~MTQueue();

    // Interface for work items
    struct Task
    {
        Task() {}
        virtual ~Task() {}
        virtual void run() = 0;
    };

    // Add a task to the queue. It should start running as soon as possible.
    // t is guaranteed to outlive this queue.
    void add(Task &t);

    // Wait for all tasks submitted via add to be empty
    void wait_for_empty();

private:
    QueueState *state_;
};
